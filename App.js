import React from 'react';
import { StyleSheet, Text, View, Image, Button, ListView } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      counter: 0,
      dataSource: ds.cloneWithRows([
        'One thing',
        'Two thing',
        'Three thing',
        'Four thing'
      ])
    };
  }
  
  increment() {
    const counter = this.state.counter + 1;
    this.setState({counter});
  }

  decrement() {
    const counter = this.state.counter - 1;
    this.setState({counter});
  }

  render() {
    let counter = this.state.counter;
    let source = {
      uri: 'http://thenexus.tv/wp-content/themes/coprime/resources/images/nx-logo-color.png'
    };
    return (
      <View style={styles.container}>
        <Text>This is a test create react native app</Text>
        <Image source={source} style={{width: 150, height: 150}} />
        <Text>counter = {this.state.counter}</Text>
        <Button title="Increment" onPress={this.increment.bind(this)} />
        <Button title="Decrement" onPress={this.decrement.bind(this)} />
        <ListView
          dataSource={this.state.dataSource}
          renderRow={ (rowData) => <Text>{rowData}</Text> }
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
